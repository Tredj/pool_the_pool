﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PoolObject
{
    public GameObject prefab;
    public int amount;
}

public class Pool : MonoBehaviour
{
    public List<PoolObject> poolObjects;
    public List<GameObject> objectsOnScene;

    private bool Game = false;
    [Header("Координаты Родительского Объекта")] Transform spawner;

    public static Pool Instanse;

    public enum Stages { Menu, Playing, Dead };
    public GameObject DeathP;
    public GameObject MenuP;

    public static Stages stages;

    private void Awake()
    {
        if (Instanse == null)
        {
            Instanse = this;
        }
        else
        {
            Destroy(this);
        }

    }

    void Start()
    {
        MenuP.SetActive(true);

        foreach (PoolObject item in poolObjects)
        {
            for (int i = 0; i < item.amount; i++)
            {

                GameObject obj = Instantiate(item.prefab, spawner);
                objectsOnScene.Add(obj);
                obj.SetActive(false);
            }
        }

    }
    public GameObject Get(string tag)
    {
        foreach (GameObject item in objectsOnScene)
        {
            if (item.CompareTag(tag) && item.activeInHierarchy == false && Game == true)
            {
                item.SetActive(true);
                return item;
            }
        }
        return null;
    }
    public GameObject Back(string teg)
    {
        foreach (GameObject item in objectsOnScene)
        {
            if (item.CompareTag(teg) && item.activeInHierarchy == true && Game == true)
            {
                item.SetActive(false);
                return item;

            }
        }
        return null;
    }

    private void Update()
    {
        switch (stages)
        {
            case Stages.Menu:
                Game = false;
                if (Input.anyKeyDown)
                {
                    Game = true;
                    MenuP.SetActive(false);
                    stages = Stages.Playing;
                }
                    break;
            case Stages.Playing:
                SpawnRandomEnemys.CoolE.CoolEnemy();
                Game = true;
                if (Enemy.HP >= 3)
                {
                    stages = Stages.Dead;
                }
                break;
            case Stages.Dead:
                Game = false;
                DeathP.SetActive(true);
                break;
        }
    }

    public void Restart()
    {
        Enemy.HP = 0;
        stages = Stages.Menu;
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }    

}
