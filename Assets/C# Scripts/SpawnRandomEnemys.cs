﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRandomEnemys : MonoBehaviour
{
    [SerializeField]
    private GameObject zone;
    public void CoolEnemy()
    {
        StartCoroutine("Eemys");
    }

    void Update()
    {
        
    }

    public IEnumerator Eemys()
    {
        
        while (true)
        {
            float a = Random.Range(1, 3);
            Pool.Instanse.Get("EnemyW");
            Enemy.EnemyBody(true,new Vector3(0, 0, 0), 150);
            yield return new WaitForSeconds(a);
        }
    }

    public static SpawnRandomEnemys CoolE;
    private void Awake()
    {
        if (CoolE == null)
        {
            CoolE = this;
        }
        else
        {
            Destroy(this);
        }

    }
}
