﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bulle : MonoBehaviour
{
    private static GameObject go;
    private static Rigidbody rb;

    void Start()
    {
        go = this.gameObject;
        rb = GetComponent<Rigidbody>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "EnemyW" || other.tag == "Dead")
        {
            Pool.Instanse.Back("Bullet");
            BulleBody(false, new Vector3(0, 0, 0), 0);
        }
    }

    public static void BulleBody(bool get, Vector3 vec, int power)
    {
        if (get == true)
        {
            go.transform.position = vec;
            rb.GetComponent<Rigidbody>().isKinematic = false;
        }
        else
        {
            rb.GetComponent<Rigidbody>().isKinematic = true;

        }
    }
}

//                item.transform.position = v;
//                item.GetComponent<Rigidbody>().isKinematic = true;
